
/*
 * GET users listing.
 */

exports.listing = function(req, res){

  req.getConnection(function(err,connection){
       
        var query = connection.query('SELECT * FROM users',function(err,rows)
        {
            
            if(err)
                console.log("Error Selecting : %s ",err );
     
   res.send({
          'status':true,
          "data":rows,
          "code":200
            });                
           
         });
         
         //console.log(query.sql);
    });
  
};

exports.add = function(req, res){
  res.render('add_customer',{page_title:"Add users - Node.js"});
};

exports.admin_login = function(req, res){
  res.render('login',{page_title:"Admin Login - Node.js"});
};

exports.admin_check_login=function(req,res){
  console.log(req.body);
  var email = req.body.email;
  var password = req.body.password;
    req.getConnection(function(err,connection){
  connection.query('SELECT * FROM users WHERE email = ?',[email], function (error, results, fields) {
  if (error) {
    // console.log("error ocurred",error);
    res.send({
      'status':false,
      "code":400,
      "failed":"error ocurred"
    })
  }else{
    //console.log('The solution is: ', results[0].password);
    if(results.length >0){
      if(results[0].password == password){
        res.send({
          'status':true,
          "data":results,
          "code":200,
          "success":"login successfull"
            });

      }
      else{
        res.send({
          'status':false,
          "code":204,
          "success":"Email and password does not match"
            });
      }
    }
    else{
      res.send({
        'status':false,
        "code":204,
        "success":"Email does not exits"
          });
    }
  }
  });
    });

};

exports.user_profile=function(req,res){
    var id = req.body.id;
req.getConnection(function(err,connection){
connection.query('SELECT * FROM users WHERE id=?',[id],function(err,result,fields){

if (result=='') {
  res.send({
        'status':false,
        "code":204,
        "success":"Data not found."
          });
}
else
{
  //console.log(result[0]);
   var data = {
            
          name    : result[0].name,
          email   : result[0].email,
          phone   : result[0].phone,
          password: result[0].password ,
          type:result[0].type,
          image:'http://localhost:7172/image/'+result[0].image
        
        };
        //console.log(data);

   res.send({
          'status':true,
          "data":data,
          "code":200,
          "success":"User Profile"
            });


}
});

});
};


exports.user_check_login=function(req,res){
  console.log(req.body);
  var username = req.body.username;
var password = req.body.password;
    req.getConnection(function(err,connection){
  connection.query('SELECT * FROM users WHERE name = ?',[username], function (error, results, fields) {
  if (error) {
    // console.log("error ocurred",error);
    res.send({
      'status':false,
      "code":400,
      "failed":"error ocurred"
    })
  }else{
    //console.log('The solution is: ', results[0].password);
    if(results.length >0){
      if(results[0].password == password){
        res.send({
          'status':true,
          "data":results,
          "code":200,
          "success":"login sucessfull"
            });

      }
      else{
        res.send({
          'status':false,
          "code":204,
          "success":"Username and password does not match"
            });
      }
    }
    else{
      res.send({
        'status':false,
        "code":204,
        "success":"Username Not Found"
          });
    }
  }
  });
    });

};


exports.login_with_phone=function(req,res){
  console.log(req.body);
  var phone = req.body.phone;
    req.getConnection(function(err,connection){
  connection.query('SELECT * FROM users WHERE phone = ?',[phone], function (error, results, fields) {
  if (error) {
    // console.log("error ocurred",error);
    res.send({
      'status':false,
      "code":400,
      "failed":"error ocurred"
    })
  }else{
    //console.log('The solution is: ', results[0].password);
    if(results.length >0){
      if(results[0].phone == phone){
        res.send({
          'status':true,
          "data":results,
          "code":200,
          "success":"login sucsessfull"
            });

      }
      else{
        res.send({
          'status':false,
          "code":204,
          "success":"Phone no not exits"
            });
      }
    }
    else{
      res.send({
        'status':false,
        "code":204,
        "success":"Phone no not exits"
          });
    }
  }
  });
    });

};

exports.edit = function(req, res){
    
    var id = req.params.id;
    
    req.getConnection(function(err,connection){
       
        var query = connection.query('SELECT * FROM users WHERE id = ?',[id],function(err,rows)
        {
            
            if(err)
                console.log("Error Selecting : %s ",err );
     
            res.render('edit_customer',{page_title:"Edit users - Node.js",data:rows});
                
           
         });
         
         //console.log(query.sql);
    }); 
};

/*Save the customer*/
exports.add_user = function(req,res){
    //console.log(__dirname);
    var input = JSON.parse(JSON.stringify(req.body));
     if (!req.files)
    {
 return res.status(400).send('No files were uploaded.');
 }
 else
 {
 
var  sampleFile = req.files.image;
var filename = sampleFile.name.replace(/[ /\\?%*:|"<>]/g, '-');
//var filename=filename + '-' + Date.now();
 //console.log(filename);
 //var pic=req.files.foo.image;
 //console.log(sampleFile.name);
  // Use the mv() method to place the file somewhere on your server
  sampleFile.mv('public/image/'+filename, function(err) {
  
  });
 }
 
//console.log(req);
    req.getConnection(function (err, connection) {
        
        var data = {
            
          name    : input.name,
          email   : input.email,
          phone   : input.phone,
          password: input.password ,
          type:input.type,
          image:sampleFile.name
        
        };
        var queryemail = connection.query('SELECT * FROM users WHERE email = ?',[data.email],function(err,result)
{

          if (result!='') {
            res.send({
          'status':false,
          "code":204,
          "success":"Email address already in use"
            });
          }
          else
          {
        var query = connection.query("INSERT INTO users set ? ",data, function(err, rows)
        {

        
  if (err)
  {
res.send({
          'status':false,
          "code":204,
          "success":"Something want wrong!"
            });
  }
  else
  {
        res.send({
          'status':true,
          "code":200,
          "success":"User Register Successfully"
            });
        } 
       
        });
 } 
        });
        
       // console.log(query.sql); get raw query
    
    });
};

exports.save_edit = function(req,res){
    
    var input = JSON.parse(JSON.stringify(req.body));
    var id = req.params.id;
     if (!req.files)
    {
 return res.status(400).send('No files were uploaded.');
 }
 else
 {
 
var  sampleFile = req.files.image;
var filename = sampleFile.name.replace(/[ /\\?%*:|"<>]/g, '-');
 //console.log(sampleFile.name);
 //var pic=req.files.foo.image;
 //console.log(sampleFile.name);
  // Use the mv() method to place the file somewhere on your server
  sampleFile.mv('public/image/'+filename, function(err) {
  
  });
 }
    req.getConnection(function (err, connection) {
        
        var data = {
            
            name    : input.name,
            password : input.password,
            email   : input.email,
            phone   : input.phone,
            id   : input.id,
            image:sampleFile.name
        
        };
        //console.log(data.id);
        
        connection.query('UPDATE `users` SET `name`=?,`email`=?,`phone`=?,`image`=? where `id`=?', [data.name,data.email, data.phone,data.image, data.id], function(err, rows)
        {
  
          if (err)
          {
             res.send({
          'status':false,
          "code":204,
          "success":"Something want wrong!"
            });
          }
          else
          {
              res.send({
          'status':true,
          "code":200,
          "success":"User Update Successfully"
            });
          }
         
          
          
        });
    
    });
};


exports.delete_user = function(req,res){

      var id = req.params.id;

     req.getConnection(function (err, connection) {
        
        connection.query("DELETE FROM users  WHERE id = ? ",[id], function(err, rows)
        {

            if (err)
            {
            res.send({
            'status':false,
            "code":204,
            "success":"Something want wrong!"
            });
            }
            else
            {
            res.send({
            'status':true,
            "code":200,
            "success":"User Deleted Successfully"
            });
            }  
   
        });
        
     });
};


exports.user_status = function(req,res){
           var input = JSON.parse(JSON.stringify(req.body));
    var data = {

      status    : input.status,
      id   : input.id  

    };
      
     req.getConnection(function (err, connection) {
        
        connection.query('UPDATE `users` SET `status`=? where `id`=?', [data.status,data.id], function(err, rows)
        {

        if (err)
        {
        res.send({
        'status':false,
        "code":204,
        "success":"Something want wrong!"
        });
        }
        else
        {
        res.send({
        'status':true,
        "code":200,
        "success":"Status Updated Successfully"
        });
        }  

        });
        
     });
};


