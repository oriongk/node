
/*
 * GET users listing.
 */

exports.list = function(req, res){

  req.getConnection(function(err,connection){
       
        var query = connection.query('SELECT * FROM users',function(err,rows)
        {
            
            if(err)
                console.log("Error Selecting : %s ",err );
     
            res.render('customers',{page_title:"users - Node.js",data:rows});
                
           
         });
         
         //console.log(query.sql);
    });
  
};

exports.add = function(req, res){
  res.render('add_customer',{page_title:"Add users - Node.js"});
};

exports.admin_login = function(req, res){
var sess;
sess = req.session;
  console.log(sess.userid);

  res.render('login',{page_title:"Admin Login - Node.js"});
};

exports.admin_check_login=function(req,res){
 
  var email = req.body.email;
var password = req.body.password;
    req.getConnection(function(err,connection){
  connection.query('SELECT * FROM users WHERE email = ?',[email], function (error, results, fields) {
  if (error) {
    // console.log("error ocurred",error);
    res.send({
      'status':false,
      "code":400,
      "failed":"error ocurred"
    })
  }else{
    //console.log('The solution is: ', results[0].password);
    if(results.length >0){
      if(results[0].password == password){
        res.send({
          'status':true,
          "data":results,
          "code":200,
          "success":"login sucessfull"
            });
        var sess;
 sess = req.session;

        
      sess.userid = results;
      console.log(sess.userid);
      }
      else{
        res.send({
          'status':false,
          "code":204,
          "success":"Email and password does not match"
            });
      }
    }
    else{
      res.send({
        'status':false,
        "code":204,
        "success":"Email does not exits"
          });
    }
  }
  });
    });

};

exports.edit = function(req, res){
    
    var id = req.params.id;
    
    req.getConnection(function(err,connection){
       
        var query = connection.query('SELECT * FROM users WHERE id = ?',[id],function(err,rows)
        {
            
            if(err)
                console.log("Error Selecting : %s ",err );
     
            res.render('edit_customer',{page_title:"Edit users - Node.js",data:rows});
                
           
         });
         
         //console.log(query.sql);
    }); 
};

/*Save the customer*/
exports.save = function(req,res){
  // the uploaded file object
    if (!req.files)
    {
 return res.status(400).send('No files were uploaded.');
 }
 else
 {
 // console.log(req.files);
 }
 var  sampleFile = req.files.foo;
 var pic=req.files.foo.name;
 //console.log(sampleFile.name);
  // Use the mv() method to place the file somewhere on your server
  sampleFile.mv('public/image/'+sampleFile.name, function(err) {
  
  });

    var input = JSON.parse(JSON.stringify(req.body));
    console.log(req);
    req.getConnection(function (err, connection) {
        
        var data = {
            
            name    : input.name,
            password : input.password,
            email   : input.email,
            phone   : input.phone,
             image:sampleFile.name
        
        };
        
        var query = connection.query("INSERT INTO users set ? ",data, function(err, rows)
        {
  
          if (err)
              console.log("Error inserting : %s ",err );
         
          res.redirect('/contact');
          
        });
        
       // console.log(query.sql); get raw query
    
    });
};

exports.save_edit = function(req,res){
    
    var input = JSON.parse(JSON.stringify(req.body));
    var id = req.params.id;
    
    req.getConnection(function (err, connection) {
        
        var data = {
            
            name    : input.name,
            password : input.password,
            email   : input.email,
            phone   : input.phone 
        
        };
        
        connection.query("UPDATE users set ? WHERE id = ? ",[data,id], function(err, rows)
        {
  
          if (err)
              console.log("Error Updating : %s ",err );
         
          res.redirect('/contact');
          
        });
    
    });
};


exports.delete_customer = function(req,res){
          
     var id = req.params.id;
    
     req.getConnection(function (err, connection) {
        
        connection.query("DELETE FROM users  WHERE id = ? ",[id], function(err, rows)
        {
            
             if(err)
                 console.log("Error deleting : %s ",err );
            
             res.redirect('/contact');
             
        });
        
     });
};


