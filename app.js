
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var fileUpload = require('express-fileupload');

//load customers route
var customers = require('./routes/customers'); 
var api = require('./routes/api'); 

var app = express();
var session = require('express-session')
var connection  = require('express-myconnection'); 
var mysqlModel = require('mysql-model');
var mysql = require('mysql');

app.use(session({secret: 'ssshhhhh'}));
app.use(fileUpload());

// all environments
app.set('port', process.env.PORT || 7172);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
//app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());

app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

/*------------------------------------------
    connection peer, register as middleware
    type koneksi : single,pool and request 
-------------------------------------------*/

app.use(
    
    connection(mysql,{
        
        host: 'localhost', //'localhost',
        user: 'root',
        password : '',
        database:'contact'

    },'pool') //or single

);


app.get('/', customers.admin_login);
app.get('/contact', customers.list);
app.get('/contact/add', customers.add);
app.post('/contact/add', customers.save);
app.get('/contact/delete/:id', customers.delete_customer);
app.get('/contact/edit/:id', customers.edit);
app.post('/contact/edit/:id',customers.save_edit);
app.get('/contact/login', customers.admin_login);
app.post('/contact/admin-login', customers.admin_check_login);

//api
app.get('/api/v1/users', api.listing);
app.post('/api/v1/add-user', api.add_user);
app.post('/api/v1/user-login', api.user_check_login);
app.post('/api/v1/user-profile', api.user_profile);

app.post('/api/v1/login-with-phone', api.login_with_phone);
app.post('/api/v1/update-profile', api.save_edit);
app.post('/api/v1/update-status', api.user_status);

app.use(app.router);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
